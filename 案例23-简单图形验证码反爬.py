import pytesseract
from PIL import Image

def handle_img(img, threshold=160):
    gray = Image.open(img).convert('L')

    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    img = gray.point(table, '1')
    return img

img = '简单图形验证码.png'
img = handle_img(img)
res = pytesseract.image_to_string(img)
print(res)
